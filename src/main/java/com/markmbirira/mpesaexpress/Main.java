package com.markmbirira.mpesaexpress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

public class Main {
	private HttpServer server;

	public Main() {
		
		try {
			int port = 8080;

			server = HttpServer.create(new InetSocketAddress(port), 0);

//			server.createContext("/confirm", new ConfirmHandler());
			server.createContext("/stkPush", new stkPushHandler());

			server.setExecutor(null);

			// start the server
			server.start();
			System.out.println("Server started");
				
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public class ConfirmHandler implements HttpHandler {

		public void handle(HttpExchange he) throws IOException {
			
			System.out.println("Request received");

			/**
			 * Buffer and store the response in a string
			 */
			BufferedReader br = new BufferedReader(new InputStreamReader(he.getRequestBody(), "UTF-8"));
			System.out.println("Request Body: " + he.getRequestBody().read());
			String line = "";
			StringBuilder buffer = new StringBuilder();
			while ((line = br.readLine()) != null) {
				buffer.append(line);
			}

			/**
			 * Once buffered, you can perform any other processing you need on the buffered
			 * response e.g. print out the response...
			 */
			System.out.println("Res: " + buffer.toString());

			/**
			 * Prepare the response, assuming no errors have occurred. Any response other
			 * than a 0 (zero) for the 'ResultCode' during Validation means an error
			 * occurred and the transaction is cancelled
			 */
			JSONObject obj = new JSONObject();
			obj.put("ResultCode", 0);
			obj.put("ResultDesc", "The service was accepted successfully");
			obj.put("ThirdPartyTransID", "1234567890");

			/**
			 * Respond to the server appropriately
			 */
			String res = obj.toJSONString();
			he.sendResponseHeaders(200, res.length());
			OutputStream os = he.getResponseBody();
			os.write(res.getBytes("UTF-8"));
			os.close();
		}
	}

	public class stkPushHandler implements HttpHandler {
		
		public void handle(HttpExchange e) throws IOException {
			System.out.println("stkpush started...");
			Map<String, String> params = queryToMap(e.getRequestURI().getQuery());

			/*
			 * Obtain the number number of the user from the Browser
			 */
			String phoneNumber = params.get("phone");
			System.out.println("Phone Number is: " + phoneNumber);
			
			ReadPropertyFile data = new ReadPropertyFile(); 
			String consumerKey = data.getConsumerKey(); 
			String consumerSecret = data.getConsumerSecret();
			String timestamp = data.getTimeStamp();
			// TODO:(Mark Mbirira) Assert that phone number gets passed along with Get Request
			String phone = params.get("phone");
		   
			MpesaSDK sdk = new MpesaSDK(consumerKey,consumerSecret);
//			String accessToken = sdk.authenticate();
			String password = generatePassword(data.getBusinessShortcode(), data.getLipaNaMpesaOnlinePassKey(), timestamp);
//			System.out.println("Base64 Password : " + password);
			sdk.STKPushSimulation(data.getBusinessShortcode(), password, timestamp, 
				  data.getTransactionType(), data.getAmount(), phone,
				  data.getPartyA(), data.getPartyB(), data.getCallbackURL(), data.getCallbackURL(),
				  data.getAccountReference(), data.getTransactionDescription());
				 
			
			/**
			 * Generate Server Response
			 */
			JSONObject obj = new JSONObject();
			obj.put("ResultCode", 0);
			obj.put("ResultDesc", "The service was accepted successfully");
			obj.put("ThirdPartyTransID", "1234567890");
			
			String res = obj.toJSONString();
			e.sendResponseHeaders(200, res.length());
			OutputStream os = e.getResponseBody();
			os.write(res.getBytes("UTF-8"));
			os.close();
		}

		public Map<String, String> queryToMap(String query) {
			Map<String, String> result = new HashMap<String, String>();
			for (String param : query.split("&")) {
				String[] entry = param.split("=");
				if (entry.length > 1) {
					result.put(entry[0], entry[1]);
				} else {
					result.put(entry[0], "");
				}
			}
			return result;
		}
		
		public String generatePassword(String shortCode, String onlinePassKey, String timeStamp) {
			String prehash = shortCode + onlinePassKey + timeStamp;
			byte[] bytes = prehash.getBytes();
			String password = Base64.getEncoder().encodeToString(bytes);
			return password;
		}
		
	}

	public static void main(String args[]) {
		Main main = new Main();
	}
}
