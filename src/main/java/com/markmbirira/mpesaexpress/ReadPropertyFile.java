package com.markmbirira.mpesaexpress;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class ReadPropertyFile {
	
	protected Properties prop = new Properties();
	protected InputStream input = ReadPropertyFile.class.getClassLoader().getResourceAsStream("com/markmbirira/mpesaexpress/data/application.properties");
	public ReadPropertyFile() throws IOException {
		prop.load(input);
//		log();
	}
	
	public String getConsumerKey() {
		return  prop.getProperty("consumerKey");
	}

	public String getConsumerSecret() {
		return prop.getProperty("consumerSecret");
	}
	
	public String getBusinessShortcode() {
		return prop.getProperty("businessShortcode");
	}
	
	public String getLipaNaMpesaOnlinePassKey() {
		return prop.getProperty("lipaNaMpesaOnlinePassKey");
	}
	
	public String getTransactionType() {
		return prop.getProperty("transactionType");
	}
	
	public String getTimeStamp() {
		DateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		return simpleDateFormat.format(date);
	}
	
	public String getAmount() {
		return prop.getProperty("amount");
	}
	
	public String getPartyA() {
		return prop.getProperty("partyA");
	}
	
	public String getPartyB() {
		return prop.getProperty("partyB");
	}
	
	public String getPhoneNumber() {
		return prop.getProperty("phoneNumber");
	}
	
	public String getCallbackURL() {
		return prop.getProperty("callbackURL");
	}
	
	public String getAccountReference() {
		return prop.getProperty("accountReference");
	}
	
	public String getTransactionDescription() {
		return prop.getProperty("transactionDesc");
	}
	
	
	public void log() { 
		System.out.println(getConsumerKey());
		System.out.println(getConsumerSecret());
		System.out.println(getBusinessShortcode());
		System.out.println(getLipaNaMpesaOnlinePassKey());
		System.out.println(getTransactionType());
		System.out.println(getAmount());
		System.out.println(getTimeStamp());
		System.out.println(getPartyA()); 
		System.out.println(getPartyB());
		System.out.println(getPhoneNumber());
		System.out.println(getCallbackURL());
		System.out.println(getAccountReference());
		System.out.println(getTransactionDescription());
	}
		 
}
